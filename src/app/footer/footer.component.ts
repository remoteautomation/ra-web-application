import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ra-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  CurYear:number;

  constructor() { }

  ngOnInit() {
    this.CurYear = new Date().getFullYear();
  }

}
