import { Component, OnInit } from '@angular/core';
import { AuthService, AuthStatus } from '../app-common/auth.service';
import { AuthRequest } from '../app-common/auth-request';
import { Router } from '@angular/router';

@Component({
  selector: 'ra-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  public submitted = false;
  public failed = false;
  public loginRequest: AuthRequest = new AuthRequest();

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.submitted) {
      return;
    }

    if (!this.loginRequest.username || this.loginRequest.username.length <= 0) {
      return;
    }

    if (!this.loginRequest.password || this.loginRequest.password.length <= 0) {
      return;
    }

    this.submitted = true;
    this.auth.Login(this.loginRequest).subscribe(
      (status: AuthStatus) => {
        switch (status) {
          case AuthStatus.Unchecked:
          case AuthStatus.Pending:
            return;
          case AuthStatus.Success:
            this.loginRequest.password = '';
            this.router.navigateByUrl('/control');
            return;
          case AuthStatus.Failed:
            this.failed = true;
            setTimeout(() => {
              this.failed = false;
            }, 5000);
            break;
        }

        this.submitted = false;
      });

  }

}
