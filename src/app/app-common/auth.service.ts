import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { LocalStorageService } from './local-storage.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthRequest } from './auth-request';
import { AuthResponse } from './auth-response';

import decode from 'jwt-decode';

export enum AuthStatus {
  Unchecked,
  Pending,
  Success,
  Failed,
  LoggedOut,
  Unavailable
}
const TOKEN_STORAGEKEY = 'AuthToken';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private status: BehaviorSubject<AuthStatus>
            = new BehaviorSubject<AuthStatus>( AuthStatus.Unchecked );
  private jwtPayload: BehaviorSubject<any>
            = new BehaviorSubject<AuthStatus>(null);

  public Status: Observable<AuthStatus> = this.status.asObservable();
  public JwtPayload: Observable<any> = this.jwtPayload.asObservable();

  constructor(private http: HttpService, private storage: LocalStorageService) {
    this.JwtPayload.subscribe();
  }

  Check(): Observable<AuthStatus> {
    const authKey = this.readToken();
    if (!authKey) {
      this.status.next(AuthStatus.Unchecked);
      return this.Status;
    }

    if (this.status.value === AuthStatus.Pending) {
      return this.Status;
    }

    this.status.next(AuthStatus.Pending);
    const subscription = this.http.Get<null>('auth/verify', 0)
      .subscribe(
        () => {
          this.status.next(AuthStatus.Success);

          subscription.unsubscribe();
        }, (error: HttpErrorResponse) => {
          switch (error.status) {
            case 401:
              this.status.next(AuthStatus.LoggedOut);
              break;
            case 0:
              this.status.next(AuthStatus.Unavailable);
              break;
            default:
              this.status.next(AuthStatus.Failed);
              break;
          }
          subscription.unsubscribe();
        });

    return this.Status;
  }

  Login(request: AuthRequest): Observable<AuthStatus> {
    if (request.username.length <= 0 || request.password.length <= 0) {
      this.status.next(AuthStatus.Failed); // Shouldn't happen if validated at login page.
      return this.Status;
    }

    this.status.next(AuthStatus.Pending);

    const subscription = this.http.Post<AuthResponse>('auth/login', request)
      .subscribe(
        (response: AuthResponse) => {
          if (!response.token) {
            this.status.next(AuthStatus.Failed);
            return;
          }

          this.setToken(response.token);
          this.status.next(AuthStatus.Success);
          subscription.unsubscribe();
        }, (error: HttpErrorResponse) => {
          switch (error.status) {
            case 0:
              this.status.next(AuthStatus.Unavailable);
              break;
            case 401:
            default:
              this.status.next(AuthStatus.Failed);
              break;
          }
          subscription.unsubscribe();
        });

    return this.Status;
  }

  Logout(): Observable<AuthStatus> {
    this.storage.Remove(TOKEN_STORAGEKEY);
    this.status.next(AuthStatus.LoggedOut);

    return this.Status;
  }

  private setToken(token: string) {
    this.storage.Set(TOKEN_STORAGEKEY, token);
    this.jwtPayload.next( decode(token) );
  }

  private readToken(): string {
    const token = this.storage.Get(TOKEN_STORAGEKEY);
    if (!token || token === '') {
      return null;
    }

    try {
      const tokenObj = decode(token);

      // Check if invalid, not applicable to this audience, or expired.
      console.log(tokenObj);
      if (!tokenObj
        || (!tokenObj.aud || tokenObj.aud !== window.location.origin)
        || (!tokenObj.exp || new Date(tokenObj.exp * 1000) <= new Date())) {
        return null;
      }

      this.jwtPayload.next(tokenObj);
      this.http.SetHeader('authorization', `Bearer ${token}`);

      return token;
    } catch {
      console.log('JWT Token invalid');
    }

    return null;
  }

}
