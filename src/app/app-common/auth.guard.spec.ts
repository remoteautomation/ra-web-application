import { TestBed, async, inject } from '@angular/core/testing';

import { UnauthedGuard } from './auth.guard';

describe('UnauthedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnauthedGuard]
    });
  });

  it('should ...', inject([UnauthedGuard], (guard: UnauthedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
