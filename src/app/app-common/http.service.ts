import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import { share, retry, catchError, delay } from 'rxjs/operators';
import { throwError } from 'rxjs';


import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  public options = {
    headers: new HttpHeaders({
      'content-type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  Get<T>(uri: string, retries: number = 3) {
    return this.http.get<T>(
        this.getUrl(uri),
        this.options
      ).pipe(
        retry(retries),
        catchError(this.catchError),
        share()
      );
  }

  Post<T>(uri: string, data: any, retries: number = 0) {
    return this.http.post<T>(
        this.getUrl(uri),
        data,
        this.options
      ).pipe(
        delay(3000),
        retry(retries),
        catchError(this.catchError),
        share()
      );
  }

  SetHeader(key: string, value: string) {
    this.options.headers
                  = this.options.headers.set(key, value);
  }


  private getUrl(uri: string) {
    return environment.api.endpoint + uri;
  }

  private catchError(error: HttpErrorResponse) {
    if (!environment.production) {
      console.error(error);
    }

    switch (error.status) {
      case 401:
        break;
    }

    return throwError(error);
  }
}
