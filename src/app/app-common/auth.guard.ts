import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService, AuthStatus } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UnauthedGuard implements CanActivate, CanActivateChild {

  constructor(private auth: AuthService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.auth.Status.pipe(map(
      (x) => {
        switch (x) {
          case AuthStatus.LoggedOut:
          case AuthStatus.Failed:
          case AuthStatus.Unavailable:
          case AuthStatus.Unchecked:
            return true;
          default:
            this.router.navigate(['/control']);
            return false;
        }
      }
    ));
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(next, state);
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthedGuard implements CanActivate, CanActivateChild {

  constructor(private auth: AuthService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.auth.Status.pipe(map(
      (x) => {
        switch (x) {
          case AuthStatus.Success:
          case AuthStatus.Unchecked:
          case AuthStatus.Pending:
            return true;
          default:
            this.router.navigate(['/login']);
            return false;
        }
      }
    ));
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(next, state);
  }
}
