import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LoadbarComponent } from './loadbar/loadbar.component';

@NgModule({
  declarations: [
    LoadbarComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    LoadbarComponent
  ]
})
export class AppCommonModule { }
