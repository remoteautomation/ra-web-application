import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  private supported = false;

  constructor() {
    if (window.localStorage) {
      this.supported = true;
    }
  }

  Get(key: string): string {
    if (!this.supported) {
      return null;
    }

    return localStorage.getItem(key);
  }

  Set(key: string, value: string) {
    if (!this.supported) {
      return;
    }

    localStorage.setItem(key, value);
  }

  Clear() {
    localStorage.clear();
  }



  get Supported() {
    return this.supported;
  }

}
