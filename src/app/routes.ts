import { Routes } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';

export const AppRoutes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'login' },
    { path: 'login', component: LoginPageComponent },
    { path: 'control', loadChildren: './control-app/control-app.module#ControlAppModule' }
];
