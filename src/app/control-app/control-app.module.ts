import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ContentHolderComponent } from './content-holder/content-holder.component';
import { SettingsPageComponent } from './settings-page/settings-page.component';
import { ServicesPageComponent } from './services-page/services-page.component';
import { ControlPageComponent } from './control-page/control-page.component';
import { RouterModule } from '@angular/router';
import { ControlRoutes } from './routes';
import { ServicePageComponent } from './service-page/service-page.component';
import { AppCommonModule } from '../app-common/app-common.module';



@NgModule({
  declarations: [
    NavigationComponent,

    DashboardPageComponent,
    ContentHolderComponent,
    SettingsPageComponent,
    ServicesPageComponent,
    ControlPageComponent,
    ServicePageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ControlRoutes),

    AppCommonModule
  ]
})
export class ControlAppModule { }
