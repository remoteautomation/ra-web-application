import { Routes } from '@angular/router';
import { ContentHolderComponent } from './content-holder/content-holder.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { SettingsPageComponent } from './settings-page/settings-page.component';
import { ServicesPageComponent } from './services-page/services-page.component';
import { ControlPageComponent } from './control-page/control-page.component';
import { ServicePageComponent } from './service-page/service-page.component';

export const ControlRoutes: Routes = [
  {
    path: '',
    component: ContentHolderComponent,
    children: [
      { path: '', pathMatch: 'full', component: DashboardPageComponent },
      { path: 'services', component: ServicesPageComponent },
      {
        path: 'service/:sId',
        component: ServicePageComponent,
        children: [
          { path: 'control/:cId', component: ControlPageComponent }
        ]
      },
      { path: 'settings', component: SettingsPageComponent }
    ]
  }
];
