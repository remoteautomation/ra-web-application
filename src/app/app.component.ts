import { Component, OnInit } from '@angular/core';
import { AuthService } from './app-common/auth.service';

@Component({
  selector: 'ra-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  constructor(private auth: AuthService) {}

  ngOnInit() {
    // Validate session at app startup.
    this.auth.Check();
  }
}
