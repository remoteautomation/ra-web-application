export const environment = {
  production: true,
  api: {
    endpoint: 'https://api.ra.wheezie.be/'
  }
};
